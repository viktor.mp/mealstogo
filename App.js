import { StatusBar as ExpoStatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView, Platform, StatusBar } from 'react-native';
import { Searchbar } from 'react-native-paper';

export default function App() {

  const [searchQuery, setSearchQuery] = useState('');

  const onChangeSearch = query => setSearchQuery(query);

  return (
    <>
    <SafeAreaView style={styles.container}>
      <View style={styles.search}>
          <Searchbar
          placeholder="Search"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
      </View>
      <View style={styles.list}>
        <Text >
          List
        </Text>
      </View>
    </SafeAreaView>
    <ExpoStatusBar style='auto'/>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
    backgroundColor: 'white'
  },
  search: {
    padding: 16,
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  list: { 
    flex: 1,
    padding: 16,
    backgroundColor: 'blue'
  }
});
